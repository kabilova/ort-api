package uz.ort.api.routers;

import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.ext.web.Router;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class RoutingService {

    protected Logger LOGGER = LoggerFactory.getLogger("RoutingService");
    protected Router router;

    public Router getRouter() {
        return this.router;
    }

    public static DeliveryOptions getDeliveryOptions(String action) {
        return (new DeliveryOptions()).addHeader("action", action);
    }
}
